use minigrep;
use minigrep::CliArgs;
use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();

    let cli_args = CliArgs::new(&args).unwrap_or_else(|err| {
        println!("Error parsing arguments: {}", err);
        process::exit(1);
    });
    if let Result::Err(e) = minigrep::run(cli_args) {
        println!("Error running program: {}", e);
        process::exit(2);
    }
}
