use std::env;
use std::error::Error;
use std::fs;

pub struct CliArgs {
    pub query: String,
    pub filename: String,
    pub is_case_insensitive: bool,
}

impl CliArgs {
    pub fn new(args: &[String]) -> Result<CliArgs, &'static str> {
        if args.len() < 3 {
            return Err("Not enough arguments");
        }
        let query = args[1].clone();
        let filename = args[2].clone();
        let is_case_insensitive = env::var("CASE_INSENSITIVE").is_ok();

        Ok(CliArgs {
            query,
            filename,
            is_case_insensitive,
        })
    }
}

pub fn run(args: CliArgs) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(args.filename)?;
    let result = if args.is_case_insensitive {
        search_case_insensitive(&args.query, &contents)
    } else {
        search(&args.query, &contents)
    };
    for matching_line in result {
        println!("{}", matching_line);
    }
    Ok(())
}

fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut result = vec![];
    for line in contents.lines() {
        if line.contains(query) {
            result.push(line);
        }
    }
    result
}

fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut result = vec![];
    let query = query.to_lowercase();
    for line in contents.lines() {
        if line.to_lowercase().contains(&query) {
            result.push(line);
        }
    }
    result
}

#[cfg(test)]
mod test {

    use super::*;

    static CONTENT: &str = "\
Rust:
safe, fast, productive.
Pick three.
I am a Duck.";

    #[test]
    fn case_sensitive() {
        let query = "duc";
        assert_eq!(vec!("safe, fast, productive."), search(query, CONTENT));
    }

    #[test]
    fn case_insensitive() {
        let query = "DuC";
        assert_eq!(
            vec!("safe, fast, productive.", "I am a Duck."),
            search_case_insensitive(query, CONTENT)
        );
    }

}
